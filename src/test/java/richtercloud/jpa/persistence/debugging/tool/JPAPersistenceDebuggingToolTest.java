/**
 * Copyright 2018 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package richtercloud.jpa.persistence.debugging.tool;

import javax.persistence.EntityManager;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
public class JPAPersistenceDebuggingToolTest {
    private final static Logger LOGGER = LoggerFactory.getLogger(JPAPersistenceDebuggingToolTest.class);

    /**
     * Test of printDebugTree method, of class JPAPersistenceDebuggingTool.
     */
    @Test
    public void testPrintDebugTree() throws Exception {
        EntityManager entityManager = mock(EntityManager.class);
        EntityA rootEntity = new EntityA();
        PKLambda checkLambda = (Object entityField) -> {
            Long retValue = ((Identifiable)entityField).getId();
            return retValue;
        };
        LOGGER.info("testing entityB field == null");
            //should be info level in order to be able to evaluate the output
        JPAPersistenceDebuggingTool.printDebugTreeLogger(rootEntity,
                entityManager,
                checkLambda,
                LOGGER);
        LOGGER.info("testing entityB field != null");
        EntityB entityB = new EntityB();
        rootEntity.setEntityB(entityB);
        JPAPersistenceDebuggingTool.printDebugTreeLogger(rootEntity,
                entityManager,
                checkLambda,
                LOGGER);
        LOGGER.info("testing transitive relationship");
        EntityC entityC = new EntityC();
        entityB.setEntityC(entityC);
        JPAPersistenceDebuggingTool.printDebugTreeLogger(rootEntity,
                entityManager,
                checkLambda,
                LOGGER);
    }
}
