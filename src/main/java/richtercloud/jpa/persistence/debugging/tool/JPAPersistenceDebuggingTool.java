/**
 * Copyright 2018 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package richtercloud.jpa.persistence.debugging.tool;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.MappedSuperclass;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import richtercloud.validation.tools.CachedFieldRetriever;
import richtercloud.validation.tools.FieldRetriever;

/**
 *
 * @author richter
 */
public class JPAPersistenceDebuggingTool {

    public static void printDebugTree(Object rootEntity,
            EntityManager entityManager,
            PKLambda pKLambda,
            OutputLambda outputLambda) throws IllegalArgumentException, IllegalAccessException {
        FieldRetriever fieldRetriever = new CachedFieldRetriever();
        Queue<Pair<Field, Object>> queue = new LinkedList<>();
        //don't put rootEntity into the queue because it's certainly not
        //persisted
        List<Field> rootEntityFields = fieldRetriever.retrieveRelevantFields(rootEntity.getClass());
        for(Field rootEntityField : rootEntityFields) {
            if(rootEntityField.get(rootEntity) != null
                    && (rootEntityField.getType().getAnnotation(Entity.class) != null
                    || rootEntityField.getType().getAnnotation(MappedSuperclass.class) != null)) {
                queue.add(new ImmutablePair<>(rootEntityField,
                        rootEntityField.get(rootEntity)));
            }
        }
        while(!queue.isEmpty()) {
            Pair<Field, Object> queueHead = queue.poll();
            assert queueHead != null;
            Object pk = pKLambda.getPK(queueHead.getValue());
            boolean check;
            if(pk == null) {
                //PK not yet set -> new or detached instance
                check = false;
            }else {
                //PK already set -> attached instance or manually set ID for not
                //yet persisted entity
                check = entityManager.find(queueHead.getKey().getType(),
                        pk) != null;
            }
            outputLambda.handle(queueHead.getKey().toGenericString()+": "+check);
            List<Field> keyFields = fieldRetriever.retrieveRelevantFields(queueHead.getValue().getClass());
            for(Field keyField : keyFields) {
                if(keyField.get(queueHead.getValue()) != null
                        && (keyField.getType().getAnnotation(Entity.class) != null
                        || keyField.getType().getAnnotation(MappedSuperclass.class) != null)
                        && keyField.get(queueHead.getValue()) != rootEntity) {
                    queue.add(new ImmutablePair<>(keyField,
                            keyField.get(queueHead.getValue())));
                }
            }
        }
    }

    public static void printDebugTreeLogger(Object rootEntity,
            EntityManager entityManager,
            PKLambda pKLambda,
            Logger logger) throws IllegalArgumentException, IllegalAccessException {
        printDebugTree(rootEntity,
                entityManager,
                pKLambda,
            outputLine -> logger.info(outputLine));
    }

    public static void printDebugTreeStdout(Object rootEntity,
            EntityManager entityManager,
            PKLambda pKLambda) throws IllegalArgumentException, IllegalAccessException {
        printDebugTree(rootEntity,
                entityManager,
                pKLambda,
            outputLine -> System.out.println(outputLine));
    }

    private JPAPersistenceDebuggingTool() {
    }
}
